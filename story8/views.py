from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

# Create your views here.

def bookfinder(request):
    return render(request, "bookfinder.html") 

def datacall(request):
    url = "https://www.googleapis.com/books/v1/volumes?q=" + request.GET['q']
    ret = requests.get(url)
    # print(ret.content)
    data = json.loads(ret.content)

    if not request.user.is_authenticated:
        i = 0 
        for x in data['items']:
            del data[i]['volumeInfo']['imageLinks']['smallThumbnail']
            i = i+1

    return JsonResponse(data, safe=False)
