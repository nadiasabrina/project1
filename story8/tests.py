from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from . import views

# Create your tests here.

class UnitTestForStory8(TestCase):
    def test_response_page(self):
        response = Client().get('/story8/bookfinder')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/story8/bookfinder')
        self.assertTemplateUsed(response, 'bookfinder.html')

    def test_func_page(self):
        found = resolve('/story8/bookfinder')
        self.assertEqual(found.func, views.bookfinder)

    # def test_api_url(self):
    #     rsp = Client().get('/story8/data/?q=frozen%202')
    #     self.assertEqual(rsp.status_code, 200)



    

