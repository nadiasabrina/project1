from django.contrib import admin
from django.urls import path, include

from . import views
app_name = 'story8'

urlpatterns = [
    path('bookfinder', views.bookfinder, name='story8_bookfinder' ), 
    path('data/', views.datacall),
   
]