from django.test import TestCase, Client
from django.urls import resolve, reverse
from . import views


# Create your tests here.

class Story10UnitTests(TestCase):
    def test_response_page_login(self):
        response = Client().get('/story9/login')
        self.assertEqual(response.status_code, 200)
    def test_response_page_register(self):
        response = Client().get('/story9/register')
        self.assertEqual(response.status_code, 200)
    def test_func_page_login(self):
        found = resolve('/story9/login')
        self.assertEqual(found.func, views.loginpage)
    def test_func_page_register(self):
        found = resolve('/story9/register')
        self.assertEqual(found.func, views.register)
    def test_logout(self):
        response = Client().get('/story9/logout')
        self.assertEqual(response.status_code, 302)

