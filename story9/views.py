from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from .forms import CreateUserForm
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.utils.decorators import method_decorator
from django.contrib.auth.hashers import make_password
import json


# Create your views here.
def loginpage(request):
    if request.method == 'POST':
        form = AuthenticationForm(data=request.POST) 
        if form.is_valid():
            user = form.get_user()
            login(request, user)    
            return redirect('story1:index')
    else: 
        form = AuthenticationForm() 
    context = {"form" : form}
    return render(request, "login.html", context) 

# def loginpage(request):
#     if request.method == 'POST':
#         username = request.POST.get('username')
#         password = request.POST.get('password1')
#         # user = authenticate(request, username=username, password=password)
#         # login(request, username, password)
#         user = authenticate(request, username=username, password=password)
#         login(request, user)
        
#     #     if user is not None:
#     #         login(request,user)
#     #         return redirect('story1:index')
#     #     else:
#     #         HttpResponse("Incorrect username or password")
    
#     return render(request, "login.html") 

def register(request):
    form = CreateUserForm();
    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            return redirect('story9:story9_login')
    context = {"form" : form}

    return render(request, "register.html", context) 

def logoutUser(request):
    logout(request)
    return redirect('story1:index')