from django.contrib import admin
from django.urls import path, include

from . import views
app_name = 'story9'

urlpatterns = [
    path('login', views.loginpage, name='story9_login'), 
    path('register', views.register, name='story9_register'),  
    path('logout', views.logoutUser, name='story9_logout'),  
]