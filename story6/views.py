from django.shortcuts import render,redirect

from .forms import KegiatanForm, AnggotaForm

from .models import KegiatanModel, AnggotaModel

# Create your views here.

def kegiatan(request):
    kegiatans = KegiatanModel.objects.all()
    anggotas = AnggotaModel.objects.all()
    
    context = {
        'kegiatans': kegiatans,
        'anggotas' : anggotas,  
    }
    return render(request, "kegiatan.html", context) 

def form_kegiatan(request):
    kegiatan_form = KegiatanForm()
    if request.method == 'POST':
        form = KegiatanForm(request.POST)
        form.save()
    
    context = {
        'data_form': kegiatan_form
    }
    return render(request, "form_kegiatan.html", context)

def form_anggota(request):
    anggota_form = AnggotaForm()
    
    if request.method == 'POST':
        form = AnggotaForm(request.POST)
        form.save()
    
    context = {
        'data_form': anggota_form
    }
    return render(request, "form_anggota.html", context)
