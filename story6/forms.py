from django import forms

from .models import KegiatanModel
from .models import AnggotaModel 

class KegiatanForm(forms.ModelForm):
    class Meta:
        model = KegiatanModel
        fields = "__all__"

class AnggotaForm(forms.ModelForm):
    class Meta:
        model = AnggotaModel
        fields = "__all__"