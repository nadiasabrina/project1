from django.db import models

# Create your models here.

class KegiatanModel(models.Model):
    nama_kegiatan = models.CharField(max_length=50)
    deskripsi = models.TextField()
    def __str__(self):
        return "{}".format(self.nama_kegiatan)

class AnggotaModel(models.Model):
    nama_anggota = models.CharField(max_length=50)
    kegiatan = models.ForeignKey(KegiatanModel, on_delete=models.CASCADE)
    def __str__(self):
        return "{}".format(self.nama_anggota)
