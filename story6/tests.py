# from django.test import TestCase, Client
# from .models import KegiatanModel
# from .models import AnggotaModel
# from .forms import AnggotaForm
# from .forms import KegiatanForm


# # Create your tests here.

# class TestKegiatan(TestCase):
#     def test_apakah_url_kegiatan_ada(self):
#         response = self.client.get('/story6/kegiatan')
#         self.assertEquals(response.status_code, 200)
    
#     def test_apakah_di_halaman_kegiatan_ada_text_Kegiatan(self):
#         response = self.client.get('/story6/kegiatan')
#         html = response.content.decode('utf8')
#         self.assertIn("Kegiatan", html)

#     def test_apakah_di_halaman_kegiatan_ada_templatenya(self):
#         response = self.client.get('/story6/kegiatan')
#         self.assertTemplateUsed(response, 'kegiatan.html')

#     def test_story6_model_kegiatan(self):
#         KegiatanModel.objects.create(nama_kegiatan='Test Kegiatan')
#         kegiatan = KegiatanModel.objects.get(nama_kegiatan='Test Kegiatan')
#         self.assertEqual(str(kegiatan), 'Test Kegiatan')
    
#     def test_story6_model_anggota(self):
#         KegiatanModel.objects.create(nama_kegiatan='Test Kegiatan')
#         AnggotaModel.objects.create(nama_anggota='Test Anggota', kegiatan=KegiatanModel.objects.get(nama_kegiatan="Test Kegiatan"))
#         anggota = AnggotaModel.objects.get(nama_anggota='Test Anggota')
#         self.assertEqual(str(anggota), 'Test Anggota')

#     def test_apakah_url_form_kegiatan_ada(self):
#         response = self.client.get('/story6/form_kegiatan')
#         self.assertEquals(response.status_code, 200)

#     def test_apakah_url_form_anggota_ada(self):
#         response = self.client.get('/story6/form_anggota')
#         self.assertEquals(response.status_code, 200)

#     def test_menambahkan_kegiatan_dari_form(self):
#         response = self.client.post('/story6/form_kegiatan', data={'nama_kegiatan':'test kegiatan', 'deskripsi' : 'test deskripsi'})
#         self.assertEqual(KegiatanModel.objects.all().count(), +1) 
#         self.assertEqual(response.status_code, 200)

#     def test_menambahkan_anggota_dari_form(self):
#         KegiatanModel.objects.create(nama_kegiatan='kegiatan')
#         response = self.client.post('/story6/form_anggota', data={'nama_anggota':'test anggota', 'kegiatan' : 1})
#         self.assertEqual(AnggotaModel.objects.all().count(), +1) 
#         self.assertEqual(response.status_code, 200)
