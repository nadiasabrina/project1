from django.contrib import admin
from .models import KegiatanModel
from .models import AnggotaModel

# Register your models here.

admin.site.register(KegiatanModel)
admin.site.register(AnggotaModel)