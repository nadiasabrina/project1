from django.contrib import admin
from django.urls import path, include

from . import views
app_name = 'story6'

urlpatterns = [
    path('kegiatan', views.kegiatan, name='story6_kegiatan' ), 
    path('form_kegiatan', views.form_kegiatan, name='story6_form_kegiatan' ),
    path('form_anggota', views.form_anggota, name='story6_form_anggota' ),
]