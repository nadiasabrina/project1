from django.db import models

# Create your models here.
class MatkulModel(models.Model):
    nama_Mata_Kuliah = models.CharField(max_length=100)
    pengajar = models.CharField(max_length=100)
    SKS = models.IntegerField()
    PILIHAN = {
        ('Gasal', 'Gasal'),
        ('Genap', 'Genap')
    }
    Semester = models.CharField(choices=PILIHAN, max_length=5)
    TAHUN = {
        ('2019', '2019'),
        ('2020', '2020')
    }
    Tahun = models.CharField(choices=TAHUN,max_length=5)
    Deskripsi = models.TextField(max_length = 500)
    Ruang_kelas = models.CharField(max_length =10)

    def __str__(self):
        return "{}.{}".format(self.id, self.nama_Mata_Kuliah)