from django import forms

from .models import MatkulModel
class MatkulForm(forms.ModelForm):
    class Meta:
        model = MatkulModel
        fields = "__all__"

# class MatkulForm(forms.Form):
#     # python data type

#     nama_Mata_Kuliah = forms.CharField(max_length = 100)
#     pengajar = forms.CharField(max_length = 100)
#     SKS = forms.IntegerField()

#     PILIHAN = {
#         ('Gasal', 'Gasal'),
#         ('Genap', 'Genap')
#     }
#     Semester = forms.ChoiceField(choices=PILIHAN)
#     TAHUN = {
#         ('2019', '2019'),
#         ('2020', '2020')
#     }
#     Tahun = forms.ChoiceField(choices=TAHUN)
#     Deskripsi = forms.CharField(widget=forms.Textarea, max_length=500)
#     Ruang_kelas = forms.CharField(max_length=10)