from django.http import HttpResponse
from django.shortcuts import render,redirect

# Create your views here.

"""
def index(request):
    return render(request,"index.html")
"""
from .forms import MatkulForm
from .models import MatkulModel

# Story 5

def matkul(request):
    matkuls = MatkulModel.objects.all()
    matkul_form = MatkulForm()
    if request.method == 'POST':
        form = MatkulForm(request.POST)
        form.save()
    #    MatkulModel.objects.create(
    #         nama_Mata_Kuliah = request.POST['nama_Mata_Kuliah'],
    #         pengajar = request.POST['pengajar'],
    #         SKS = request.POST['SKS'],
    #         Semester = request.POST['Semester'],
    #         Tahun = request.POST['Tahun'],
    #         Deskripsi = request.POST['Deskripsi'],
    #         Ruang_kelas = request.POST['Ruang_kelas']
    #     )
    context = {
        'data_form': matkul_form,
        'matkuls':matkuls
    }
    return render(request, "matkul.html", context)

def delete(request,delete_id):
    MatkulModel.objects.filter(id=delete_id).delete()
    return redirect('story5:story5_matkul')

def jadwal(request):
    matkuls = MatkulModel.objects.all()
    matkul_form = MatkulForm()
    
    context = {
        'data_form': matkul_form,
        'matkuls':matkuls
    }

    return render(request, "jadwal.html", context) 

def detail(request, id_matkul):
    matkuls = MatkulModel.objects.get(id=id_matkul)
    
    context = {
        'matkuls':matkuls
    }
    return render(request, "detail_matkul.html", context)

def index(request):
    return render(request, "story1/index.html") 