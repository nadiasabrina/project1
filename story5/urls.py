from django.contrib import admin
from django.urls import path, include

from . import views
app_name = 'story5'

urlpatterns = [
    path('matkul', views.matkul, name='story5_matkul' ), 
    path('jadwal', views.jadwal, name='story5_jadwal' ), 
    path('delete/<int:delete_id>', views.delete, name='delete'),
    path('detail/<int:id_matkul>', views.detail, name ='detail'),
]