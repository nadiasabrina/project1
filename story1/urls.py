from django.contrib import admin
from django.urls import path, include

from . import views
app_name = 'story1'

urlpatterns = [
    path('profile/', views.profile, name='story1_profile' ),
    path('', views.index, name='index'),
]

