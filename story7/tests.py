from django.test import TestCase
from django.test import Client
from django.urls import resolve
from . import views
# Create your tests here.

class UnitTestForStory7s(TestCase):
    def test_response_page(self):
        response = Client().get('/story7/informasi')
        self.assertEqual(response.status_code, 200)

    def test_template_used(self):
        response = Client().get('/story7/informasi')
        self.assertTemplateUsed(response, 'accordion.html')

    def test_func_page(self):
        found = resolve('/story7/informasi')
        self.assertEqual(found.func, views.accordion)


